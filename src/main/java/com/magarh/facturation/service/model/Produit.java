package com.magarh.facturation.service.model;

import lombok.Data;
import org.springframework.cloud.openfeign.FeignClient;

@Data
public class Produit {
    private Long id;
    private String nom;
    private double prix;
    private int quantite;
}
