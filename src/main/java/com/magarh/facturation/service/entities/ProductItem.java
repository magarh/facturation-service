package com.magarh.facturation.service.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.magarh.facturation.service.model.Produit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor  @AllArgsConstructor @ToString
public class ProductItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private double prix;
    private double quantite;
    @ManyToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Facture facture;
    @Transient
    private Produit produit;
    private Long productId;
}
