package com.magarh.facturation.service.web;


import com.magarh.facturation.service.entities.Facture;
import com.magarh.facturation.service.feign.ClientRestClient;
import com.magarh.facturation.service.feign.ProductItemRestClient;
import com.magarh.facturation.service.model.Client;
import com.magarh.facturation.service.model.Produit;
import com.magarh.facturation.service.repository.FactureRepository;
import com.magarh.facturation.service.repository.ProductItemRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FacturationController {
    private FactureRepository factureRepository;
    private ProductItemRepository productItemRepository;
    private ProductItemRestClient productItemRestClient;
    private ClientRestClient clientRestClient;

    public FacturationController(FactureRepository factureRepository, ProductItemRepository productItemRepository,
                                 ProductItemRestClient productItemRestClient, ClientRestClient clientRestClient) {
        this.factureRepository = factureRepository;
        this.productItemRepository = productItemRepository;
        this.productItemRestClient = productItemRestClient;
        this.clientRestClient = clientRestClient;
    }

    @GetMapping(path = "/fullFacture/{id}")
    public Facture getFacture(@PathVariable(name = "id") Long id){

        Facture facture =factureRepository.findById(id).get();
        Client client = clientRestClient.getClientById(facture.getClienId());
        facture.setClient(client);
        facture.getProductItem().forEach(p->{
            Produit produit =productItemRestClient.getProductById(p.getProductId());
            p.setProduit(produit);
        });
        return facture;
    }
}
