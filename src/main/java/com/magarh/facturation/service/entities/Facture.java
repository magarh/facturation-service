package com.magarh.facturation.service.entities;


import com.magarh.facturation.service.model.Client;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor @AllArgsConstructor @ToString
public class Facture {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date dateFaturation;
    private Long clienId;
    @Transient
    private Client client;
    @OneToMany(mappedBy = "facture")
    private Collection<ProductItem> productItem;

}
