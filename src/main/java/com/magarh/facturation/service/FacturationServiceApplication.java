package com.magarh.facturation.service;

import com.magarh.facturation.service.entities.Facture;
import com.magarh.facturation.service.entities.ProductItem;
import com.magarh.facturation.service.feign.ClientRestClient;
import com.magarh.facturation.service.feign.ProductItemRestClient;
import com.magarh.facturation.service.model.Client;
import com.magarh.facturation.service.model.Produit;
import com.magarh.facturation.service.repository.FactureRepository;
import com.magarh.facturation.service.repository.ProductItemRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.PagedModel;

import java.util.Date;
import java.util.Random;

@SpringBootApplication
@EnableFeignClients
public class FacturationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FacturationServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner start(FactureRepository factureRepository, ProductItemRepository productItemRepository,
					ClientRestClient clientRestClient, ProductItemRestClient productItemRestClient){
		return  args -> {
			System.out.println("Recupération du client grâce à OpenFein");
			Client client =clientRestClient.getClientById(1L);
			System.out.println(client.getId());
			System.out.println(client.getNom());
			System.out.println(client.getEmail());
			Facture facture =factureRepository.save(new Facture(null, new Date(), client.getId(),null, null));
			PagedModel<Produit> pageProducts =productItemRestClient.pageProducts(0, 1);
			pageProducts.forEach(p->{
				ProductItem productItem = new ProductItem();
				productItem.setPrix(p.getPrix());
				productItem.setQuantite((1+(Math.random()*100)));
				productItem.setFacture(facture);
				productItem.setProductId(p.getId());
				productItemRepository.save(productItem);
			});

		};
	}

}
