package com.magarh.facturation.service.feign;

import com.magarh.facturation.service.model.Produit;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.PagedModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "PRODUIT-SERVICE")
public interface ProductItemRestClient {
    @GetMapping(path = "/produits")
    PagedModel<Produit> pageProducts(@RequestParam(name = "page") int page, @RequestParam(name = "size") int size);

    @GetMapping(path = "/produits/{id}")
    public Produit getProductById(@PathVariable(name = "id") Long id);
}
