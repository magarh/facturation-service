package com.magarh.facturation.service.model;

import lombok.Data;
import org.springframework.cloud.openfeign.FeignClient;


@Data
public class Client {
    private Long id;
    private String nom;
    private String email;
}
